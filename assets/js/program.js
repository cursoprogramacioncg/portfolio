//ESTO DEBE IR ABAJO
//document.querySelector("form").addEventListener("submit", handleSubmit);

const handleSubmit = (e) => {   //ARROW FUNCTION, PARÁMETRO E QUE ES EL BOTÓN
    console.log("Esoty dentro");
  e.preventDefault()    //PREVIENE EL FUNCIONAMIENTO PREDETERMINADO QUE ES RECARGAR TODA LA PÁGINA
  let myForm = document.getElementById('contacto-portfolio');   //RECIBE EL ELEMENTO HTML
  let formData = new FormData(myForm)   //CREA UN OBJETO DEL FORMULARIO
  /*La API Fetch proporciona una interfaz JavaScript para acceder y manipular partes 
  del canal HTTP, tales como peticiones y respuestas. 
  También provee un método global fetch() (en-US) que proporciona una forma fácil y 
  lógica de obtener recursos de forma asíncrona por la red.*/
  fetch('/', {          
    method: 'POST',     // METODO DE CONEXION
    headers: { "Content-Type": "application/x-www-form-urlencoded" }, //ENCABEZADO REQUERIDO POR NETLIFY
    body: new URLSearchParams(formData).toString()  //EN BODY CARGA EL STRING DE LA URL, CON LOS DATOS QUE GUARDARÁ
  }).then(() => /*console.log('Form successfully submitted')*/ 
        document.querySelector("#resultado").innerHTML = "Mensaje enviado!")
    .then(setTimeout(() => {
        document.querySelector("#resultado").innerHTML = "";
        document.querySelector("#contacto-portfolio").reset();      //Y LUEGO LO PONEMOS ACÁ
    }, 5000))
    // .then(document.querySelector("#contacto-portfolio").reset()) //LO PROBAMOS PRIMERO CON OTRO THEN
    .catch((error) => alert(error));
};

document.querySelector("form").addEventListener("submit", handleSubmit);
